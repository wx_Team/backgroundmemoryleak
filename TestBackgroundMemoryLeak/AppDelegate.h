//
//  AppDelegate.h
//  TestBackgroundMemoryLeak
//
//  Created by wx on 11/9/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

