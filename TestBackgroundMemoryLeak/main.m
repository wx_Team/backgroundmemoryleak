//
//  main.m
//  TestBackgroundMemoryLeak
//
//  Created by wx on 11/9/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
